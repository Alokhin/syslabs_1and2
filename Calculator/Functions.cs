﻿using System;
using System.Collections.Generic;

namespace AntlrParser.Math
{
    public static class Functions
    {
        public static Dictionary<string, Func<double, double>> OneArgumentFunctions = 
            new Dictionary<string, Func<double, double>>()
            {
                { "SIN", System.Math.Sin },
                { "COS", System.Math.Cos },
                { "LOG", System.Math.Log } // log with e base
            };

        public static Dictionary<string, Func<double, double, double>> TwoArgumentFunctions =
            new Dictionary<string, Func<double, double, double>>()
            {
                { "LOG", System.Math.Log }, // log with custom base
                { "NTH_ROOT", (x, n) => System.Math.Pow(x, 1.0 / n) }
            };
    }
}
