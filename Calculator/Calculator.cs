﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr4;
using Antlr4.Runtime;

namespace AntlrParser.Math
{
    public static class Calculator
    {
        public static double Evaluate(string expression)
        {
            var lexer = new BasicMathLexer(new AntlrInputStream(expression));
            lexer.RemoveErrorListeners();
            lexer.AddErrorListener(new ThrowExceptionErrorListener());

            var tokens = new CommonTokenStream(lexer);
            var parser = new BasicMathParser(tokens);

            var tree = parser.compileUnit();

            var visitor = new MathVisitor();

            return visitor.Visit(tree);
        }
    }
}
