﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr4.Runtime.Tree;

namespace AntlrParser.Math
{
    class MathVisitor : BasicMathBaseVisitor<double>
    {
        public override double VisitCompileUnit(BasicMathParser.CompileUnitContext context)
        {
            // There can only ever be one expression in a compileUnit. The other node is EOF.
            return Visit(context.expression(0));
        }

        public override double VisitNumber(BasicMathParser.NumberContext context)
        {
            return double.Parse(context.GetText());
        }

        public override double VisitAddition(BasicMathParser.AdditionContext context)
        {
            var left = WalkLeft(context);
            var right = WalkRight(context);

            return left + right;
        }

        public override double VisitSubtraction(BasicMathParser.SubtractionContext context)
        {
            var left = WalkLeft(context);
            var right = WalkRight(context);
      
            return left - right;
        }

        public override double VisitMultiplication(BasicMathParser.MultiplicationContext context)
        {
            var left = WalkLeft(context);
            var right = WalkRight(context);

            return left * right;
        }

        public override double VisitDivision(BasicMathParser.DivisionContext context)
        {
            var left = WalkLeft(context);
            var right = WalkRight(context);

            return left / right;
        }

        public override double VisitExponentiation(BasicMathParser.ExponentiationContext context)
        {
            var left = WalkLeft(context);
            var right = WalkRight(context);

            return System.Math.Pow(left, right);
        }

        public override double VisitParenthesis(BasicMathParser.ParenthesisContext context)
        {
            return Visit(context.GetChild(1));
        }

        public override double VisitFunction(BasicMathParser.FunctionContext context)
        {
            var children = new List<IParseTree>();

            for (var i = 0; i < context.ChildCount; i++)
            {
                children.Add(context.GetChild(i));
            }

            var function = children[0].GetText();

            var args = children
                .GetRange(2, children.Count - 3)
                .Where((x, i) => i % 2 == 0)
                .Select(a => Visit(a))
                .ToList();

            if (args.Count == 1 && Functions.OneArgumentFunctions.ContainsKey(function))
            {
                var func = Functions.OneArgumentFunctions[function];
                return func(args[0]);
            }
            if (args.Count == 2 && Functions.TwoArgumentFunctions.ContainsKey(function))
            {
                var func = Functions.TwoArgumentFunctions[function];
                return func(args[0], args[1]);
            }
        
            throw new ArgumentException($"Can't find function '{function}' with provided number of arguments {args.Count}");
        }

        public override double VisitConstant(BasicMathParser.ConstantContext context)
        {
            var name = context.children[0].GetText();

            var constant = Constants.Get(name);

            if (constant == null)
            {
                throw new ArgumentException($"Can't find constant '{context}'");
            }

            return constant.Value;
        }

        private double WalkLeft(BasicMathParser.ExpressionContext context)
        {
            return Visit(context.GetRuleContext<BasicMathParser.ExpressionContext>(0));
        }

        private double WalkRight(BasicMathParser.ExpressionContext context)
        {
            return Visit(context.GetRuleContext<BasicMathParser.ExpressionContext>(1));
        }
    }
}
