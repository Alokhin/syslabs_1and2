﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AntlrParser.Math;

namespace CalculatorTests
{
    [TestClass]
    public class EvaluateTests
    {
        private static double DELTA = 0.005;

        [TestMethod]
        public void OrderedOperation()
        {
            var expr = "1 + 6 - 2 * 3 / 2";
            Assert.AreEqual(4, Calculator.Evaluate(expr));
        }

        [TestMethod]
        public void SimpleAddition()
        {
            Assert.AreEqual(2, Calculator.Evaluate("1 + 1"));
        }

        [TestMethod]
        public void RepeatedAddition()
        {
            Assert.AreEqual(10, Calculator.Evaluate("1 + 2 + 3 + 4"));
        }

        [TestMethod]
        public void SimpleSubtraction()
        {
            Assert.AreEqual(2, Calculator.Evaluate("4 - 2"));
        }

        [TestMethod]
        public void RepeatedSubtraction()
        {
            Assert.AreEqual(5, Calculator.Evaluate("10 - 3 - 2"));
        }

        [TestMethod]
        public void SimpleMultiplication()
        {
            Assert.AreEqual(4, Calculator.Evaluate("2 * 2"));
        }

        [TestMethod]
        public void RepeatedMultiplication()
        {
            Assert.AreEqual(8, Calculator.Evaluate("2 * 2 * 2"));
        }

        [TestMethod]
        public void SimpleDivision()
        {
            Assert.AreEqual(2, Calculator.Evaluate("4 / 2"));
        }

        [TestMethod]
        public void RepeatedDivision()
        {
            Assert.AreEqual(2, Calculator.Evaluate("8 / 2 / 2"));
        }

        [TestMethod]
        public void DoubleTest()
        {
            Assert.AreEqual(2.4, Calculator.Evaluate("2.4"));
        }

        [TestMethod]
        public void Parenthesis()
        {
            Assert.AreEqual(10, Calculator.Evaluate("2*(3+2)"));
            Assert.AreEqual(55, Calculator.Evaluate("(3+8)*(3+2)"));
        }

        [TestMethod]
        public void SimpleExponentiation()
        {
            Assert.AreEqual(1000, Calculator.Evaluate("10 ^ 3"));
            Assert.AreEqual(1024, Calculator.Evaluate("2 ^ 10"));
            Assert.AreEqual(0.01, Calculator.Evaluate("0.1 ^ 2"), DELTA);
            Assert.AreEqual(1, Calculator.Evaluate("0.1 ^ 0"));
        }

        [TestMethod]
        public void ScientificNotation()
        {
            Assert.AreEqual(123, Calculator.Evaluate("1.23e+2"));
            Assert.AreEqual(123, Calculator.Evaluate("1.23E+2"));
            Assert.AreEqual(5000, Calculator.Evaluate("5e+3"));
            Assert.AreEqual(5000, Calculator.Evaluate("5E+3"));
            Assert.AreEqual(0.123, Calculator.Evaluate("1.23e-1"));
            Assert.AreEqual(0.123, Calculator.Evaluate("1.23E-1"));
            Assert.AreEqual(0.5, Calculator.Evaluate("5e-1"));
            Assert.AreEqual(0.5, Calculator.Evaluate("5E-1"));
        }

        [TestMethod]
        public void Functions()
        {
            Assert.AreEqual(0, Calculator.Evaluate("SIN(0)"));
            Assert.AreEqual(1, Calculator.Evaluate("COS(0)"));

            Assert.AreEqual(4, Calculator.Evaluate("LOG(81, 3)"));
            Assert.AreEqual(10, Calculator.Evaluate("LOG(1024, 2)"));

            Assert.AreEqual(2, Calculator.Evaluate("NTH_ROOT(16, 4)"));
            Assert.AreEqual(5, Calculator.Evaluate("NTH_ROOT(625, 4)"));
        }

        [TestMethod]
        public void Constants()
        {
            Assert.AreEqual(Math.E, Calculator.Evaluate("E"), DELTA);
            Assert.AreEqual(Math.PI, Calculator.Evaluate("PI"), DELTA);
        }

        [TestMethod]
        public void FunctionAndConst()
        {
            Assert.AreEqual(0, Calculator.Evaluate("SIN(PI)"), DELTA);
            Assert.AreEqual(-1, Calculator.Evaluate("COS(PI)"), DELTA);

            Assert.AreEqual(1, Calculator.Evaluate("SIN(PI/2)"), DELTA);
            Assert.AreEqual(0, Calculator.Evaluate("COS(PI/2)"), DELTA);

            Assert.AreEqual(0.5, Calculator.Evaluate("SIN(PI/6)"), DELTA);
            Assert.AreEqual(Math.Sqrt(3)/2, Calculator.Evaluate("COS(PI/6)"), DELTA);

            Assert.AreEqual(1, Calculator.Evaluate("LOG(E)"), DELTA);
            Assert.AreEqual(2, Calculator.Evaluate("LOG(E^2)"), DELTA);
            Assert.AreEqual(10, Calculator.Evaluate("LOG(E^10)"), DELTA);
        }

        [TestMethod]
        public void ComplexExample()
        {
            var expectedResult = Math.PI * Math.Pow(Math.E, Math.Cos(Math.Sin(Math.PI / 5))) +
                                 Math.Pow(0.3 * Math.Log(Math.E - 0.5, 3), 4) + 2e3;

            var exampleInParserSyntax = "PI * E ^ COS(SIN(PI/5)) + (0.3 * LOG(E - 0.5, 3)) ^ 4 + 2e3";

            var actualResult = Calculator.Evaluate(exampleInParserSyntax);

            Assert.AreEqual(expectedResult, actualResult, DELTA);          
        }
    }
}
