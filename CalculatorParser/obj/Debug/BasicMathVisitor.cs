//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     ANTLR Version: 4.3
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from D:\Projects\Antlr4Calculator\CalculatorParser\BasicMath.g4 by ANTLR 4.3

// Unreachable code detected
#pragma warning disable 0162
// The variable '...' is assigned but its value is never used
#pragma warning disable 0219
// Missing XML comment for publicly visible type or member '...'
#pragma warning disable 1591

namespace AntlrParser.Math {
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using IToken = Antlr4.Runtime.IToken;

/// <summary>
/// This interface defines a complete generic visitor for a parse tree produced
/// by <see cref="BasicMathParser"/>.
/// </summary>
/// <typeparam name="Result">The return type of the visit operation.</typeparam>
[System.CodeDom.Compiler.GeneratedCode("ANTLR", "4.3")]
[System.CLSCompliant(false)]
public interface IBasicMathVisitor<Result> : IParseTreeVisitor<Result> {
	/// <summary>
	/// Visit a parse tree produced by the <c>Function</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFunction([NotNull] BasicMathParser.FunctionContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>Parenthesis</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitParenthesis([NotNull] BasicMathParser.ParenthesisContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>Multiplication</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitMultiplication([NotNull] BasicMathParser.MultiplicationContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>Addition</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitAddition([NotNull] BasicMathParser.AdditionContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>Subtraction</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitSubtraction([NotNull] BasicMathParser.SubtractionContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>Number</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitNumber([NotNull] BasicMathParser.NumberContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="BasicMathParser.compileUnit"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitCompileUnit([NotNull] BasicMathParser.CompileUnitContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>Constant</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitConstant([NotNull] BasicMathParser.ConstantContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>Division</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitDivision([NotNull] BasicMathParser.DivisionContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>Exponentiation</c>
	/// labeled alternative in <see cref="BasicMathParser.expression"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitExponentiation([NotNull] BasicMathParser.ExponentiationContext context);
}
} // namespace AntlrParser.Math
