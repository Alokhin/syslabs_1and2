﻿using System;
using System.Linq;
using AntlrParser.Math;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = string.Empty;

            do
            {
                PrintInfoMessage();

                input = Console.ReadLine();

                Console.WriteLine($"{input} = {Calculator.Evaluate(input)}");

            } while (!string.IsNullOrWhiteSpace(input));
        }

        static void PrintInfoMessage()
        {
            Console.WriteLine();
            Console.WriteLine("Enter math expression to solve (leave it empty to exit)");
            Console.WriteLine($"You can use these consts : {string.Join(", ", Constants.GetAllNames())}");
            Console.WriteLine($"And these functions : {string.Join(", ", Functions.OneArgumentFunctions.Select(f => $"{f.Key}(x)"))} " +
                              $"and {string.Join(", ", Functions.TwoArgumentFunctions.Select(f => $"{f.Key}(x, y)"))}");
        }
    }
}
