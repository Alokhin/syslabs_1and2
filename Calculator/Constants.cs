﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AntlrParser.Math
{
    public static class Constants
    {
        private static Dictionary<string, double> _consts = new Dictionary<string, double>()
        {
            { "PI", System.Math.PI},
            { "E", System.Math.E}
        };

        public static double? Get(string name)
        {
            if (_consts.TryGetValue(name, out var value))
            {
                return value;
            }

            return null;
        }

        public static IEnumerable<string> GetAllNames()
        {
            return _consts.Select(c => c.Key).AsEnumerable();
        }
    }
}
