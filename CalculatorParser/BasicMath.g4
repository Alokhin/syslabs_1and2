grammar BasicMath;

/*
 * Parser Rules
 */

compileUnit : expression+ EOF;

expression :
	WORD LPAREN (expression (COMMA expression)*) RPAREN #Function
	| LPAREN expression RPAREN #Parenthesis
    | expression POWER expression #Exponentiation
    | expression MULTIPLY expression #Multiplication
	| expression DIVIDE expression #Division
	| expression ADD expression #Addition
	| expression SUBTRACT expression #Subtraction
	| NUMBER #Number
	| WORD #Constant
	; 

/*
 * Lexer Rules
 */

fragment DIGIT
    : ('0' .. '9')
    ;

COMMA
    : ','
    ;


fragment DECIMAL_SEPARATOR
    : '.'
    ;

LPAREN
    : '('
    ;

RPAREN
    : ')'
    ;

fragment LETTER
    : ('a' .. 'z')
    | ('A' .. 'Z')
	| '_'
    ;

WORD
    : LETTER LETTER*
    ;

NUMBER
    : DIGIT DIGIT* (DECIMAL_SEPARATOR DIGIT DIGIT*)? (('E' | 'e') ('+' | '-')? DIGIT DIGIT*)?
    ;


INT : ('0'..'9')+;
POWER: '^';
MULTIPLY : '*';
DIVIDE : '/';
SUBTRACT : '-';
ADD : '+';

WS : [ \t\r\n] -> channel(HIDDEN);
